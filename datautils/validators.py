from . import export


@export
def dob_validate(dob, min_age):
    """
    Validate date of birth based on min age
    :param dob: datetime object
    :min_age: minimum age according to which dob is considered to be correct
    :return:
    0 - no value
    1 - value does not pass min_age criterion
    9 - ok value
    -1 - error exception during execution
    """
    if not dob:
        return 0
    try:
        from datetime import date
        today = date.today()
        age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
        if age < min_age:
            return 1
        return 9
    except:
        return -1


@export
def email_validate(email, check_mx=True):
    """
    TODO - add check against c3dss.fct_et_subscribers
    need to check '!Invalid!'
    """
    from validate_email import validate_email
    if not email:
        return 0
    if email.startswith('!Invalid!'):
        return 1
    if check_mx:
        try:
            if validate_email(email, check_mx=True):
                return 9
        except:
            pass
    try:
        if validate_email(email, check_mx=False):
            return 8
    except:
        pass
    return 2


@export
def txt_invalid(x, len_min, len_max):
    """
    Returns True if text is invalide based on min/max number of required length:
    not_vl(strt,4,200) - street address should be between 4 and 200 characters
    Used for convenience for all() and any() checks when validating combine address quality
    """
    if not x:
        return True
    if len(str(x).strip()) < len_min:
        return True
    if len(str(x).strip()) > len_max:
        return True
    return False


@export
def pcode_validate(pcode):
    """
    Postcode quality
    """
    if not pcode:
        return 0
    pcode = str(pcode)
    if len(pcode.strip()) != 4:
        return 1
    pcode = "".join([x for x in pcode if x.isdigit()])
    if len(pcode) != 4:
        return 1
    return 9


@export
def addr_validate(strt, suburb, state, post_code):
    """
    Checks quality of whole address as combination of strt, suburb, state, post_code fields.
    0 - no value
    1 - value does not pass min_age criterion
    9 - ok value
    """
    if not any([strt, suburb, state, post_code]):
        return 0
    if not_vl(strt,4,200) or all([not_vl(suburb,2,200), not_vl(post_code,4,4)]) or all([not_vl(post_code, 4,4), not_vl(state,2,40)]):
        return 1
    return 9  #2


@export
def phone_validate(ph):
    """
    This function cleans provided phone number and tries to guess if it's an AU mobile or landline
    :param ph: str
    :return: dict {"mob": str, "land": str}
    """
    ph = "".join([x for x in str(ph) if x.isdigit()])
    if not ph or len(ph) < 8:
        return {}
    if ph.startswith("04") and len(ph) == 10:
        return {"mob":ph}
    if ph.startswith("614") and len(ph) == 11:
        return {"mob":"0" + ph[2:]}
    if ph.startswith("61") and len(ph) == 11:
        return {"land":"0" + ph[2:]}
    if ph.startswith("0") and len(ph) == 10:
        return {"land": ph}
    if not ph.startswith("0") and len(ph) == 8:
        #yz: leaving it as 8 symbols so land number can be read based on local state context
        return {"land":ph}
    return {}


@export
def blcard_validate(card_n):
    """
    Checks if the customers Beauty Loop card number is consistent.
    :param card_n: Beauty Loop card number.
    :return:
     0 card number does not exist
     9 card number is consistent
     1 card number isn't consistent
    """
    if not card_n:
        return 0
    if card_n.isdigit() and len(str(card_n)) == 8:
        return 9
    return 1
