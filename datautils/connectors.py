from . import export


@export
def pg_eng(host="pg.host", port=5432, db="pg", user=None, passwd=None,
            sslmode="verify-ca", sslrootcert=None, pool_size=10):
    import os
    if not user:
        user = os.environ["PG_USER"]
        passwd = os.environ["PG_PASS"]
    import os, getpass
    from sqlalchemy import create_engine
    from sqlalchemy.pool import QueuePool
    if not sslrootcert:
        ## if not provided - is set to location of aws rds cert in datalab:
        sslrootcert = "/home/{}/.jupyter/rds-combined-ca-bundle.pem".format(getpass.getuser())
    con_str = "postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{db}".format(user=user,
        passwd=passwd, host=host, port=str(port), db=db)
    try:
        eng = create_engine(con_str, encoding="utf-8", poolclass=QueuePool,
            pool_size=pool_size, max_overflow=0, echo=False,
            connect_args={"sslmode":sslmode, "sslrootcert":sslrootcert})
        return eng
    except Exception as e:
        print(e)
        return "problem connecting to {db}: {e}".format(db=db, e=str(e))


@export
def azsql_eng(host="", schema="", user=None, passwd=None, pool_size=10, driver='ODBC+Driver+17+for+SQL+Server'):
    """
    Function that creates SQLAlchemy engine object for MS SQL Server on Azure
    pyodbc module has to be used in place of pymssql  
    requires installation of msodbcsql17   
    """
    import os
    import pyodbc
    from sqlalchemy import create_engine
    if not user:
        user = os.environ["DB_USER"]
        passwd = os.environ["DB_PASS"]
    con_str = 'mssql+pyodbc://{user}:{passwd}@{host}:1433/{schema}?driver={driver}'.format(
        user=user, passwd=passwd, host=host, schema=schema, driver=driver)
    try:
        return create_engine(con_str, encoding="utf-8", pool_size=pool_size, max_overflow=0, echo=False)
    except Exception as e:
        return "problem connecting to %s: %s" % (host, str(e))


@export
def mssql_eng(host="mssql.host", schema="mssql", user=None, passwd=None, pool_size=10):
    """
    Function that creates SQLAlchemy engine object for an abstract SQL Server 
    """
    import os, pymssql
    from sqlalchemy import create_engine
    if not user:
        user = os.environ["MSSQL_USER"]
        passwd = os.environ["MSSQL_PASS"]
    con_str = "mssql+pymssql://{user}:{passwd}@{host}:1433/{schema}".format(user=user, passwd=passwd, host=host, schema=schema)
    try:
        return create_engine(con_str, encoding="utf-8", pool_size=pool_size, max_overflow=0, echo=False)
    except Exception as e:
        return "problem connecting to %s: %s" % (host, str(e))


@export
def mysql_eng(host="mysql.host", port=3306, db="dss", user="DSS_USER"):
    """
    Function that creates SQLAlchemy engine object for MySQL database.
    """
    import pymysql
    from sqlalchemy import create_engine
    con_str = "mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8".format(user=os.environ[user],
    passwd=os.environ[user.replace("USER","") + "PASS"], host=host, port=str(port), db=db)

    try:
        return create_engine(con_str, encoding="utf-8")
    except Exception as e:
        return "problem connecting to database: " + str(e)


@export
def ora_eng(host="ora.host", schema="ora"):
    """
    Function that creates SQLAlchemy engine object for RetailPro.
    """
    import os
    import cx_Oracle as cx_oracle
    from sqlalchemy import create_engine
    con_str = "oracle+cx_oracle://%s:%s@%s:1521/%s?charset=utf8" % (os.environ["RPRO_USER"], os.environ["RPRO_PASS"], host, schema)
    try:
        return create_engine(con_str, encoding="utf-8")
    except Exception as e:
        return "problem connecting to %s: %s" % (host, str(e))


@export
def rs_eng(host="localhost", port=5432, db=None, user=None, passwd=None):
    """
    Function that creates SQLAlchemy engine object for RedShift.
    """
    import psycopg2, os
    from sqlalchemy import create_engine
    if not user:
        user = os.environ["PG_USER"]
        passwd = os.environ["PG_PASS"]
    con_str = "postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{db}".format(user=os.environ[user],
        passwd=os.environ[user.replace("USER","") + "PASS"], host=host, port=str(port), db=db)
    try:
        return create_engine(con_str, encoding="utf-8")
    except Exception as e:
        return "problem connecting to {db}: {e}".format(db=db, e=str(e))


@export
def ses_send_email(email_from="Test Sender <sender@domain.com>", emails_to=["receiver@domain.com"], email_subj="Test Subj",
    email_body="Test Body", format="text"):
    import os, boto.ses
    user = os.environ["SES_USER"]
    key = os.environ["SES_KEY"]
    con = boto.ses.connect_to_region(
            'us-east-1',
            aws_access_key_id=user,
            aws_secret_access_key=key)
    con.send_email(email_from, email_subj, email_body, emails_to, format=format)
    return 1
