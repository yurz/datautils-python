from . import export


@export
def time_diff(x, max_date, period="month"):
    from relativedelta import relativedelta
    diff = relativedelta.relativedelta(max_date, x)
    if period == "month":
        return diff.years * 12 + diff.months + 1  # round up
    if period == "year":
        return diff.years + 1  # round up
    return "Error - Provide 'period' attribute"


@export
def time_stamp(*tm):
    """
    Return time as a sting. Handy for writing to logs.
    """
    import datetime
    if tm:
        return tm[0].strftime("%Y-%m-%d %H:%M:%S")
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


@export
def id_gen(size=3):
    import string
    import random
    chars = string.ascii_letters
    return ''.join(random.choice(chars).lower() for x in range(size))


@export
def proper_name(txt):
    """
    Converting table and column names to proper lower case format with underscore.
    """
    import re
    bad_chars = "\"'(){}[]"
    #Capitals - to _underscore:
    #first Capital:
    txt = re.sub('$(.)([A-Z][a-z]+)', r'\1_\2', txt)
    #rest:
    txt = re.sub('([a-z0-9])([A-Z])', r'\1_\2', txt).lower()
    #Spaces - to _underscore:
    txt = re.sub(r"[\s+\.+\-+]", '_', txt.strip())
    #remove some bad characters:
    for char in bad_chars:
        txt = txt.replace(char, "")
    #replace unicode identifier
    txt = txt.replace("\ufeff", "")
    txt = txt.encode('ascii', 'ignore').decode("utf8")
    return txt


@export
def fix_df_cols(df):
    cl = []
    for c in df.columns:
        c = proper_name(c)
        cl.append(c)
    df.columns = cl
    return df


@export
def dthandler(obj):
    import datetime
    from pandas import isnull
    from numpy import integer, floating
    if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, integer):
        return int(obj)
    elif isinstance(obj, floating):
        return float(obj)
    elif isnull(obj) or str(obj).lower() == 'nan':
        return "-"
    else:
        return ""


@export
def sql(env, sql, **kwargs):
    sql = sql.replace("&&env.", env)
    for k in kwargs:
        sql = sql.replace("&&" + k, kwargs[k])
    return sql


@export
def iso_datetime_now(loc="Australia/Melbourne"):
    """
    :param loc: str - local time zone, defaults to Australia/Melbourne.
    :return: str - Current datetime as ISO datetime format string without microseconds.
    Suitable for Retail Pro XML files.
    """
    import datetime, pytz
    return datetime.datetime.now(pytz.timezone(loc)).replace(microsecond=0).isoformat()


@export
def df_datatypes_fix(df):
    """
    Function to fix various potential data type compatibility issues.
    """
    # deal with datetime64 to_csv() bug
    have_datetime64 = False
    dtypes = df.dtypes
    for i, k in enumerate(dtypes.index):
        dt = dtypes[k]
        if str(dt.type)=="<type 'numpy.datetime64'>":
            have_datetime64 = True
    if have_datetime64:
        for i, k in enumerate(dtypes.index):
            dt = dtypes[k]
            if str(dt.type)=="<type 'numpy.datetime64'>":
                df[k] = [v.to_pydatetime() for v in df[k]]
    return df


@export
def df_to_memfile(df, header=True,  delimiter="\t"):
    """
    Convert Pandas DataFrame to in-memory file object.
    """
    from io import StringIO
    memfile = StringIO()
    df = df_datatypes_fix(df)
    df.to_csv(memfile, sep=delimiter, header=header, index=False, na_rep="")
    memfile.seek(0)
    return memfile


@export
def pg_copy_from(data_obj, table_name, cur, delimiter="\t", columns=None):
    from pandas import DataFrame
    """
    Append data into existing postgresql table using COPY.
    data_obj:: file object or pandas.DataFrame object.
    columns:: list of columns names. when data_obj is a DataFarme - columns can be
        left empty in which case DataFarme columns used.
    """
    if isinstance(data_obj, DataFrame):
        if not columns:
            columns = data_obj.columns
        data_obj = df_to_memfile(data_obj, delimiter=delimiter)

    cmd = """COPY {table_name}({columns}) FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER '{delimiter}')""".format(table_name=table_name,
          columns=",".join(columns), delimiter=delimiter)
    cur.copy_expert(cmd, data_obj)
    cnt = cur.rowcount

    print("copied {} rows".format(cnt))
    return cnt


@export
def pg_copy_to(file_obj, sql, cur, delimiter="\t"):
    """
    Export data to CSV using Postgres copy_to feature.
    file_obj:: file object for saving CSV export.
    sql:: sql for extracting data.
    cur:: db cursor. 
    """
    cmd = """COPY ({sql}) TO STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER '{delimiter}')""".format(sql=sql, delimiter=delimiter)
    cur.copy_expert(cmd, file_obj)
    cnt = cur.rowcount
    print("copied {} rows".format(cnt))
    return cnt


@export
def ora_in1000(sql, chunk_size=999, lst=None, con=None, are_str=False):
    """
    Addresses oracle limitation for number of items in 'in (...)' statement.
    Takes sql, list of items (supposedly > 1000) and connection/engine.
    Returns pandas dataframe.
    sql has to be in form 'select ... in ({}) ...'
    or 'select ... in ('{}') ...' - in which case pass are_str=True
    """
    from pandas import read_sql, concat
    df_lst = []
    for ch in [lst[x:x+chunk_size] for x in range(0, len(lst), chunk_size)]:
        if are_str:
            in_str = "','".join([str(x) for x in ch])
        else:
            in_str = ",".join([str(x) for x in ch])
        df_lst.append(read_sql(sql.format(in_str), con=con, coerce_float=False))
    return concat(df_lst)


@export
def add_ga_script(html_str=None, app_name=None, ga_code='UA-11111111-1', custom_hash=''):
    if not html_str:
        raise "Provide html_str"

    if not app_name:
        raise "Provide app_name"

    ga = """
    <script>
    // GA
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '%s', 'auto');

      $('.ga_event').on('click', function() {
          ga('send', 'event', this.href, 'click');
      });

      ga('set', 'dimension1', "%s");
      ga('set', 'dimension2', "%s");

    // add src and ip dimensions:
        var src = "-";
        var ind = window.location.href.indexOf('?');
        var attr = [];
        if (ind > 5) {
            attr = window.location.href.slice(ind + 1).split('&');
            if (typeof attr[0] !== "undefined") {
                 src = attr[0];
                }
        }
        var username = $('#server-data').attr('username');
        ga('set', 'dimension3', username);

        var ip = "-";
        $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
          function(json) {
           ip = json.ip;
           ga('set', 'dimension4', ip);
           ga('send', 'pageview');
          }
        ).error(function() { ga('send', 'pageview'); });
    </script>
    """ % (ga_code, custom_hash, app_name)

    return html_str.replace("</body>", "\n{}</body>".format(ga))


@export
def rpro_build_customer_xml(data, empty="prevent"):
    """
    Builds XML file for RetailPro import from list of dictionaries with arbitrary number of attributes
    :param data: list of dictionaries, can be df.to_dict(orient="records")
    :param empty:
    Caution - Make sure your data does not contain stringified "None", "NaN" etc. objects!
        "prevent" - default behaviour, raises a ValueError on attempt to insert an empty attribute;
        "skip" - an empty attribute will not be recorded in XML output
        "force" - an empty attribute will be recorded in XML thus blanking the original one in RetailPro
    :return: str with XML contant ready to be printed or written to a file
    """
    from lxml import etree
    mandatory = ("sbs_no", "cust_sid", "modified_date")
    document = etree.Element('DOCUMENT')
    customers = etree.SubElement(document, 'CUSTOMERS')
    for sid in data:
        if not all([k in sid for k in mandatory]):
            raise ValueError("One or more mandatory keys missing in the record: " + str(sid))
        customer = etree.SubElement(customers, 'CUSTOMER')
        for k in sid:
            if len(str(sid[k])) == 0:
                if empty == "skip":
                    continue
                elif empty == "prevent":
                    raise ValueError("Empty attribute in: " + str(sid))
                elif empty == "force":
                    pass
                else:
                    raise AttributeError("Unknown option for attribute 'empty': " + str(empty))
            customer.attrib[k] = str(sid[k])
    return etree.tostring(document, pretty_print=True, xml_declaration=True, encoding='UTF-8')


@export
def df_to_redash(df_orig, index_to_col=False):
    """
    https://github.com/getredash/redash/issues/2078
    """
    import numpy as np
    df = df_orig.copy()
    if index_to_col:
        df.reset_index(inplace=True)
    result = {'columns': [], 'rows': []}
    conversions = [
        {'pandas_type': np.integer, 'redash_type': 'integer',},
        {'pandas_type': np.inexact, 'redash_type': 'float',},
        {'pandas_type': np.datetime64, 'redash_type': 'datetime', 'to_redash': lambda x: x.strftime('%Y-%m-%d %H:%M:%S')},
        {'pandas_type': np.bool_, 'redash_type': 'boolean'},
        {'pandas_type': np.object, 'redash_type': 'string'}
    ]
    labels = []
    for dtype, label in zip(df.dtypes, df.columns):
        for conversion in conversions:
            if issubclass(dtype.type, conversion['pandas_type']):
                result['columns'].append({'name': label, 'friendly_name': label, 'type': conversion['redash_type']})
                labels.append(label)
                func = conversion.get('to_redash')
                if func:
                    df[label] = df[label].apply(func)
                break
    result['rows'] = df[labels].replace({np.nan: None}).to_dict(orient='records')
    return result


@export
def redash_to_df(result, col_to_index=False):
    """
    https://github.com/getredash/redash/issues/2078
    """
    import pandas as pd
    conversions = [
        {'redash_type': 'datetime', 'to_pandas': lambda x: pd.to_datetime(x, infer_datetime_format=True)},
        {'redash_type': 'date', 'to_pandas': lambda x: pd.to_datetime(x, infer_datetime_format=True)},
    ]
    df = pd.DataFrame.from_dict(result['rows'], orient='columns')
    labels = []
    for column in result['columns']:
        label = column['name']
        labels.append(label)
        for conversion in conversions:
            if conversion['redash_type'] == column['type']:
                func = conversion.get('to_pandas')
                if func:
                    df[label] = df[label].apply(func)
                break
    df = df[labels]
    if col_to_index and labels:
        df.set_index(labels[0], inplace=True)
    return df
