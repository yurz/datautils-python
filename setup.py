import os

from setuptools import setup, find_packages

def read(*paths):
    """Build a file path from *paths* and return the contents."""
    with open(os.path.join(*paths), 'r') as f:
        return f.read()

setup(
    name='datautils',
    version='20190502.14',
    py_modules=['connectors', 'validators', 'utils'],
    url='https://bitbucket.org/yurz/datautils-python',
    description='List of handy data manipulation functions',
    long_description=(read('README.md')),
    license='MIT',
    author='Yuri Zhylyuk',
    author_email='yuri@zhylyuk.com',
    include_package_data=True,
    classifiers=[
        'Natural Language :: English',
        'Operating System :: OS Independent but Linux Opinionated :)',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    packages=find_packages(exclude=['tests*', 'misc*']),
    install_requires=['numpy', 'pandas', 'datetime']
)
